import numpy as np
from parabolic2D import parabolic2D
import matplotlib.pyplot as plt
from datetime import datetime
import time
from mpl_toolkits.mplot3d import Axes3D

znak = 1

mu = 0.

R = 1
L = 1
# def f(x,y,t):
#     return x*(R-x)*y*(L-y)+2.*t*(x*(R-x)+y*(L-y))
# def v(x,y):
#     return 0.
# def u(x,y,t):
#     return t*x*(R-x)*y*(L-y)
n_r = 100
n_l = 100
tau = 0.00004

print('Число узлов вдоль Or = ', n_r, ', вдоль Oz = ', n_r)

start_time = datetime.now()

h_r = R/(n_r-0.5)
h_z = L/n_l

tEnd = 0.01

grid_r = np.linspace(h_r/2, R, n_r)
grid_z = np.linspace(0, L, n_l)

ut = np.zeros((n_r,n_l), 'float')
ff = np.zeros((n_r,n_l), 'float')
vv = np.zeros((n_r,n_l), 'float')
phi = np.zeros((n_r,n_l), 'float')
D = np.ones((n_r,n_l), 'float')


for i in range(0,n_r):
    for j in range(0,n_l):
        ut[i,j] = (1-(grid_r[i]/R)**2)*grid_z[j]*(L-grid_z[j])/L
        phi[i,j] = (1-grid_r[i]**2)*200*(grid_z[j]-1)
        # print(i, j, ut[i,j])
        # ut[i,j] = u(grid_r[i],grid_z[j],tEnd)
        ff[i,j] = 4/R**2*(grid_z[j]*(L-grid_z[j]/L) \
                          +2.*(1-(grid_r[i]/R)**2))/L
        # ff[i,j] = f(grid_r[i],grid_z[j],tEnd)
        
tauList = [0.1, 0.05, 0.025]

t0 = 0.



# n_iter = 2000

vv = ut.copy()

# print('Нач. приближение \n', vv)

# for tau in tauList:
for Kiter in range(11):
    tau = min(tau, tEnd - t0)
    U = parabolic2D(grid_r, grid_z, ff, vv, znak, mu, phi, D, tEnd, tau)
#(r, z, f, y_0, znak, mu, phi, D, tEnd, tau)
err = np.amax(np.abs(vv-U))
vv = U.copy()
    

time.sleep(5)

print('Время выполнения =',  datetime.now() - start_time)

print('tau =', tau, 'error = %e' % (np.amax(np.abs(ut-U))))


# print(ut)

# print(ff)
    
# 2D graphic

fig = plt.figure()
X, Y = np.meshgrid(grid_z, grid_r)
ax = fig.add_subplot(111) # параметры контейнера для вывода графика
plt.title('Приближенное решение')
plt.contourf(X, Y, U, cmap=plt.cm.binary)
plt.xlabel('z')
plt.ylabel('r')
plt.colorbar()
plt.show()

# 3D graphic

fig = plt.figure()
X, Y = np.meshgrid(grid_z, grid_r)
ax = fig.add_subplot(111, projection='3d') # параметры контейнера для вывода графика
plt.title('Приближенное решение')
ax.set_xlabel('z')
ax.set_ylabel('r')
ax.set_zlabel('u')
ax.plot_surface(X, Y, U, rstride=1, cstride=1, cmap='binary')
plt.show()


fig = plt.figure()
X, Y = np.meshgrid(grid_z, grid_r)
plt.title('Точное решение')
plt.contourf(X, Y, ut, cmap=plt.cm.binary)
ax.set_xlabel('z')
ax.set_ylabel('r')
plt.colorbar()
plt.show()

# 3D graphic

fig = plt.figure()
X, Y = np.meshgrid(grid_z, grid_r)
ax = fig.add_subplot(111, projection='3d') # параметры контейнера для вывода графика
plt.title('Точное решение')
ax.set_xlabel('z')
ax.set_ylabel('r')
ax.set_zlabel('u')
ax.plot_surface(X, Y, ut, rstride=1, cstride=1, cmap='binary')
plt.show()