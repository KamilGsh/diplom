
import math 
import numpy as np
def fsolve(FF,N,M,r,z,Ne,Ni,hr,hz,omega_relax =1.8):

   print("вход в подпрограмму ")
   print(FF)

   FF1 = FF.copy()# np.zeros((N,M))

   eps0 = 8.85*10**(-12) #электрическая постоянная
   e = 1.6*10**(-19) #заряд электрона
   #f = e/eps0 * (Ni-Ne)
   f = np.ones((N,M)) * (100)
   print('f = ',f)

   # f2 = np.ones((N*M)) 

   omega_relax =1.8
   
   
   
   
   d = np.ones((N*M)) #формируем диагональ матрицы СЛАУ
   l1 = np.zeros((N*M)) 
   l2 = np.zeros((N*M)) 
   for j in range(1,M-1):
       for i in range(1,N-1):
           k = i+j*N
           d[k] = 2/hr**2 +2/hz**2
           l1[k] = -(r[i]-hr/2)/(r[i]*hr**2)
           l2[k] = -1/hz**2
       l1[int((j+1)*N-1)] = -1/hr**2

   print()
   print('d',d)
   print()
   print('l1',l1)
   print()
   print('l2',l2)
   print()
   tol = 1.e-2
   rn_finish =1.e32

   kiter =0
   #N_iter = N*M # кол - во итераций ну пока что остановилсиь на двух
   N_iter = 5


   for p in range(N_iter):
      norm_dif = 0
      rn = 0.
      print('rn = ',rn)
    #граничное условие для левой и правой стенки 
      
      """ на сколько я помню граничные условия мы задавали в Main"""

      #for j in range(1,M-1):
      #граничное условие для левой стенки    
        # print("FF[1][j],FF[0][j],hr,f[0][j],FF[0][j-1],FF[0][j+1],hz")
        # print(FF[1][j],FF[0][j],hr,f[0][j],FF[0][j-1],FF[0][j+1],hz)
        # rr = -2*(FF[1][j] -FF[0][j] )/hr**2  - f[0][j] -(FF[0][j-1]-2*FF[0][j] + FF[0][j+1])/hz**2  
        # rn = rn+rr**2
        # print ('i = ',0,'j = ', j, 'rr = ',rr,'rn = ',rn,'z[j] = ',z[j])
        # FF[0][j] = FF[0][j]  - (omega_relax*rr)/(d)
        # norm_dif = max(norm_dif,abs((omega_relax*rr)/(d)))

      print("Расчет")

      # граничное условие для правой стенки

         #rr = (FF[N][j] -FF[N-1][j] )/hr**2 *(R-hr/2) - 0.5*f[N][j]
         #rn = rn+rr**2
         #FF[N][j] = FF[N][j]  - omega_relax/hr**2
   
    # граничное условие для врехней границы
      #for i in range(N2+1,N-1):
        # rr = (FF[i][M] -FF[i][M-1] )/hz**2  - 0.5*f[i][M]
        # rn = rn+rr**2
        # FF[i][M] = FF[i][M]  - omega_relax/hz**2
      
      resid = np.zeros((N*M))
      corrector = np.zeros((N*M))


      for j in range(1,M-1):
            # Граничные условия второго рода слева  
            rr = (FF[0][j]*(2/hr**2) - FF[1][j]*(2/hr**2))\
                - 1/hz**2 * FF[0][j-1] + 2/hz**2 * FF[0][j] - 1/hz**2 * FF[0][j+1]\
                - f[0][j]
            resid[j*N] = rr
            rn = rn+rr**2
            print('rr невязка левые граница', rr,'\n' )
            FF1[0][j] = FF[0][j] - (omega_relax*rr)/d[j*N]
            norm_dif = max(norm_dif,abs((omega_relax*rr)/(d[j*N])))
            
            # Обходим внутренние узлы
            for i in range(1,N-1):
               A1 = -(FF[i-1][j]*(r[i]- hr/2) -2.*FF[i][j]*r[i] + FF[i+1][j]*(r[i]+hr/2)) / (hr**2 * r[i]) 
               A2 = -(FF[i][j-1] -2.*FF[i][j] + FF[i][j+1])/hz**2
               print('A1 = ',A1 )
               print('A2 = ',A2)
               print('f[i][j] = ',f[i][j])
               rr = A1  + A2 - f[i][j]
               corrector[i+j*N] = rr - omega_relax*(l1[i+j*N-1]*corrector[i+j*N -1] 
                                                    - l2[i+(j-1)*N]*corrector[i+(j-1)*N])/d[i+j*N]
              
               resid[i+j*N] = rr
               rn = rn+rr**2
               print ('i = ',i,'j = ', j, 'rr = ',rr,'rn = ',rn)
               #FF[i][j] = FF[i][j] - omega_relax*rr/d[i+j*N]
               FF1[i][j] = FF[i][j] - omega_relax*corrector[i+j*N]
               norm_dif = max(norm_dif,abs(omega_relax*corrector[i+j*N]))

            # справа
            rr = (FF[N-1][j] - FF[N-2][j])*2/ hr**2 - f[N-1][j]\
              -1/hz**2*(FF[N-1][j-1]-2*FF[N-1][j]+FF[N-1][j+1])
            corrector[(j+1)*N-1] = rr - omega_relax*(l1[(j+1)*N-2]*corrector[(j+1)*N-2] 
                                                    - l2[j*N-1]*corrector[j*N-1])/d[(j+1)*N-1]
            print('невязка правой границы',rr)
            resid[(j+1)*N-1] = rr
            rn = rn+rr**2
            FF1[N-1][j] = FF[N-1][j] - (omega_relax*corrector[(j+1)*N-1])
            norm_dif = max(norm_dif,abs(omega_relax*corrector[(j+1)*N-1]))
      
      ddd=np.max(np.abs(FF-FF1))
      err_c = norm_dif/ddd
      FF=FF1.copy()
      rn = rn*hr*hz
      kiter = p
      rn_finish = rn
      #print("итерация номер ", k, " невязка = ", rn," погрешность = ", err_c)
      if err_c<tol and rn_finish <tol**2:
          break
      
   print("число итераций =  ", kiter+1)
   print("невязка приближенного решения",rn)
   print("относительная разность, norm_dif = ", norm_dif)
   return FF,rn

   
   
   
   
   
  
    
            
       
    

      
      
    
    
    


    
      

        
    


'''
     условия нижние
for i in border_ground:
    k = i[0]
    l = i[1]
    FF[k,l] = U1

for i in border_top:
    k = i[0]
    l = i[1]
    FF[k,l] = U2

 граничные условия верхние
N2 = (r1 - hr)//hr
for i in range(N2+1):
    FF[i][M-1] = U2




for i in border_axes:
    k = i[0]
    l = i[1]
 на оси
a = np.zeros((N))
b = np.zeros((N))
c = np.zeros((N))

for j in range(1,M-1):
    
    b[j] = 
    c[j] = 
    a[j] = 
    FF[0][j] = (f[0][j] + ((r[0]+hr/2)/(r[0]*hr**2))*FF[1][j]+ FF[0][j-1]/hr**2 + FF[0][j+1]/hz**2)/(2/(r[0]*hr**2) + 2/hz**2)


 на правой стороне

'''
