import numpy as np
import matplotlib.pyplot as plt
from NI import NI
from NE import NE
from Fsolve_1 import fsolve
def EE(x,y): #ni ne na vhod
    return 1
def DE(x,y):
    pass
def DI(x,y):
    pass
def VI(x,y):
    pass



N =3
M =3
R=1
'''радиус электрода'''
r1 = 0.005
L=1
hr = R/(N-0.5)
hz = L/(M-1)
tEnd =1
tau = 0.01
''' setka '''
r = np.linspace(hr/2,R,N)
z = np.linspace(0,L,M)



''' двумерные массивы'''
Er=np.zeros((N,M))
Ez=np.zeros((N,M))
ne = np.zeros((N,M))
ni = np.zeros((N,M))
De = np.zeros((N,M))
Di = np.zeros((N,M))
f= np.zeros((N,M)) # массив для правой части
'''Поток'''
Gi = np.zeros((N-1,M-1))
Ge = np.zeros((N-1,M-1))
'''Потенциал'''
FF = np.zeros((N,M))
''' constants'''
eps0 = 8.85*10**(-12)
e = 1.6*10**(-19)
p =1.e5
NE0 = 1.e18
E0 = 5.e3
F0 =1500.
U1 = 100.
U2 = 0.
mue = 1
mui = 1
'''начальные приближения'''
for j in range(0,M):
    for i in range(0,N):
        ne[i][j] = (1-(r[i]/R)**2)*z[j]*(z[j]-L)/L**2 * NE0
        Er[i][j] = E0*((r[i]/R))
        Ez[i][j] = E0*(z[j]/L)
        FF[i][j] = ((z[j]*(z[j]-L))*F0)/(L**2 *(r[i]/R))

ni = ne.copy()
''' Решение уравнения Пуассона'''
f = e/eps0 * (ni-ne)

'''Граничные условия '''
# border_ground =[]
# border_wall_right =[]
# border_top = []
# border_axes = []
# for i in range(N):
#     border_ground.append([i,0])
#     border_top.append([i,M-1])

# for i in range(M):
#     border_wall_right.append([N-1,i])
    
# for i in range(1,M-1):
#     border_axes.append([0,i])

# for i in border_ground:
#     k = i[0]
#     l = i[1]
#     FF[k,l] = U1


# На нижней границе
for i in range(N):
    FF[i][0] = U1

# На верхней границе
if r1-hr <=0:
    FF[0][M-1] = U2
    for i in range(2,N):
        FF[i][M-1] == 2.
else:
    
    N2 = int((r1 - hr)/hr)
    for i in range(N2+1):
        FF[i][M-1] = U2
    for i in range(N2+2,N):
      FF[i][M-1] = 2.

while tEnd-tau>0 :
    FF = fsolve(FF,N,M,r,z,ne,ni,hr,hz)



    



    










    


