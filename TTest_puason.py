import numpy as np
import matplotlib.pyplot as plt
"""
Предполагалось что это будет main  тут должны будут сойтись все функции
"""

"""
Недописанные функции по концентрации ионов и электронов

"""

from NI import NI # Функции по расчету концентрации ионов и Электронов
from NE import NE
from Fsolve_1 import fsolve # Функция по решению уравнения Пуассона
import math 

"""
Недописанные функции по напряженности электрического поля , коэффициентам диффузии
"""
def EE(x,y): #ni ne na vhod
    return 1
def DE(x,y):
    pass
def DI(x,y):
    pass
def VI(x,y):
    pass

''' constants'''
eps0 = 8.85*10**(-12)
e = 1.6*10**(-19)
p =1.e5
NE0 = 1.e18
E0 = 5.e3
F0 =1500.
U1 = -100.
U2 = 0.
mue = 1
mui = 1
alpha = 1
ne_0 = 1.e18
"""  
Количество точек по осям
"""
N =3 # Число узлов
M =3 # Число узлов
"""
Линейные размеры
"""
R=1
'''радиус электрода'''
#r1 = 0.005
r1=R
"""
Высота емкости
"""
L=1

"""
азимутальный и радиальный шаг
"""

hr = R/(N-0.5)
hz = L/(M-1)

"""
Шаг по времени, и время в течение которого , должно было вестись расчет, но это недописанная программа и поэтому они не используются 
"""
tEnd =1
tau = 1
''' setka '''
r = np.linspace(hr/2,R,N)
z = np.linspace(0,L,M)
print(r)
print(z)


''' двумерные массивы   компоненты радиальной и азимтульной напряженности электрического поля  '''
Er=np.zeros((N,M))
Ez=np.zeros((N,M))
ne = np.zeros((N,M))
ni = np.zeros((N,M))
De = np.zeros((N,M))
Di = np.zeros((N,M))
f= np.zeros((N,M)) # массив для правой части
'''Поток'''
Gi = np.zeros((N-1,M-1))
Ge = np.zeros((N-1,M-1))
'''Потенциал'''
FF = np.zeros((N,M))


""" начальное приближение"""
for j in range(0,M):
    for i in range(0,N):
        ne[i][j] = ne_0*(1-(r[i]/R)**2)*(z[j]*(L-z[j]))
        Er[i][j] = E0*((r[i]/R))
        Ez[i][j] = E0*(z[j]/L)
        FF[i][j] = -50*z[j]**2
        #r[i] + z[j]*U1/L
        #FF[i][j] = math.exp(-alpha*r[i]*r[i])*(U2/L * z[j] + U1)
print('r = ',r)
print(z)
""" Граничные условия"""
for i in range(N):
    FF[i][0] = 0 #низ
for i in range(N):
    FF[i][M-1] = -50 #вверх
#for j in range(M): #задаем В Fsolve так же как и лвеой
   # FF[N-1][j] = R*R + z[j]*z[j] #право10

print("Точное решение")
print(FF)
""" точное решение у нас совпадает с начальным приближением"""
# for j in range(1,M-1):
#     for i in range(1,N-1):
#         FF[i,j] = 0
print("Начальное приближение")
print(FF)
ni = ne.copy()
''' Решение уравнения Пуассона , правые части'''
f = e/eps0 * (ni-ne)


#for j in range(M):
   # for i in range(N):
        #f[i][j] = 4*math.exp(-2*r[i]*r[i])*(alpha**2 *r[i]*r[i] - alpha)*(U2/L * z[j]+ U1)
       # f[i][j] = -6


'''Граничные условия . Недоделанная задумка по нумерации точек . Пока что непонятно как использовать '''
border_ground =[]
border_wall_right =[]
border_top = []
border_axes = []
for i in range(N):
    border_ground.append([i,0])
    border_top.append([i,M-1])

for i in range(M):
    border_wall_right.append([N-1,i])
    
for i in range(1,M-1):
    border_axes.append([0,i])

#for i in border_top:
   # k = i[0]
    #l = i[1]
    #FF[k][l] = U1
#for i in range(N):
   # FF[i][0] = U1

N2 = int((r1 - hr)/hr)
#for i in range(N2+1):
   # FF[i][M-1] = U2

print('решение')
t = 0
while t<= tEnd:
    
    FF,nv = fsolve(FF,N,M,r,z,ne,ni,hr,hz)
    t= tau+t
print(FF.transpose())

